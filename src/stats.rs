pub struct Stat
{
    value : u8
}

impl Stat
{
    /// Creates a Stat value with a given number
    ///
    /// # Examples
    ///
    /// ```
    /// use cyberpunk2020::Stat;
    /// let stat = Stat::new(2);
    /// ```
    ///
    /// # Panics
    ///
    /// The function panics if the value given does not match the range [2,10]
    ///
    /// ```rust,should_panic
    /// use cyberpunk2020::Stat;
    /// Stat::new(1);
    /// Stat::new(11);
    /// ```
    pub fn new(value: u8) -> Stat
    {
        if value < 2 || value > 10
        {
            panic!("Stat Out Of Bounds");
        }

        Stat{
            value
        }
    }
}

///The Different types of Stat that a character can have
pub enum CharacterStat
{
    ///This is a measure of your problem solving ability; figuring out problems, noticing things, 
    ///remembering information. Almost every character type will need a high Intelligence, with 
    ///Netrunners and Corporates requiring the highest of all.
    INT(Stat),
    ///This is a combined index, covering not only your basic dexterity, but also how your level 
    ///of physical coordination will affect feats of driving, piloting, fighting and athletics.
    ///Characters who intend to engage in a great deal of combat (such as Solos, Nomads or Rockerboys)
    ///should always invest in the highest possible Reflex.
    REF(Stat),
    ///This is an index of how well you relate to hardware and other technically orientated things.
    ///In *Cyberpunk*, the ability to use and repair technology is of paramount importance--TECH 
    ///will be the Stat used when fixing, repairing and attempting to use unfamiliar tech. While all
    ///characters should have a decent Tech Stat, potential Techies should always opt for the highest
    ///possible score in this area.
    TECH(Stat),
    ///This index measures how well the character stands up to stress, fear, pressure, physical pain 
    ///and/or torture. In determining your willingness to fight on despite to fight on despite wounds
    ///or your fighting ability under fire, *Cool*(CL) is essential. It is also the measure of how "together"
    ///your character is and how tough he appears to others. Rockerboys and Fixers should always have a high Cool,
    ///with Solos and Nomads having the highest of all.
    COOL(Stat),
    ///This is how good-looking you are. In *Cyberpunk*, it's not enough to be good--you have to look 
    ///good while you're doing it(attitude is everything). Attractiveness is especially important to 
    ///Medias and Rockerboys, as being good-looking is part of the job.
    ATTR(Stat),
    ///This is the intangiable "something" that throws the balance of events into your favour. Your luck
    ///represents how many points you may use each game to influence the outcome of a critical event. To
    ///use Luck, you may add any or all of the points of luck a character has to a critical die roll(declaring
    ///your intention beforehand) until all your Luck stat is used up. Luck is always restored at the end of each
    ///game session.
    LUCK(Stat),
    ///This is an index of how fast your character can run(important in combat). The higher your Movement Allowance
    ///(MA), the more distance you can cover in a turn. This stat also informs your *RUN* and *LEAP* stats.
    MA(Stat),
    ///Strength, Endurance and Consititution are all based on the characters Body Type. Body Type determines how much
    ///damage you can take in wounds, how much you can lift or carry, how far you can throw, how well you recover from
    ///shock, and how much additional damage you cause with physical attacks. Body Type is important to all character
    ///types, but to Solos, Rockerboys and Nomads most of all.
    BODY(Stat),
    ///This Stat represents how well you relate to other living things--a measure of charisma and sympathetic emotions.
    ///In a world of alienated, future-shocked survivors, the ability to be "human" can no longer be taken for granted.
    ///Empathy(EM) is critical when leading, convincing, seducing or percieving emotional undercurrents. Empathy is 
    ///also a measure of how close he/she is to the line between human and cold blooded cyber-monster.
    EMP(Stat)
}


#[cfg(test)]
mod test
{
    use super::Stat;

    #[test]
    fn test_new_in_bounds()
    {
        let stat = Stat::new(2);
        assert_eq!(stat.value, 2);
    }

    #[test]
    #[should_panic]
    fn test_new_low_out_of_bounds()
    {
        Stat::new(0);
    }

    #[test]
    #[should_panic]
    fn test_new_high_out_of_bounds()
    {
        Stat::new(11);
    }
}